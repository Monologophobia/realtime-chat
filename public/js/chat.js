var url = "http://127.0.0.1:8000/";

// get chat_id
var chat_id = window.chat.id;

// load css
var link  = document.createElement('link');
link.rel  = "stylesheet";
link.type = "text/css";
link.href = url + "css/iframe.css";
document.body.appendChild(link);

// load container div
var div = document.createElement('div');
div.id  = 'live-chat';

// create a header for the container
var online = document.createElement('div');
online.id  = "online";
online.innerHTML = "Live Chat";
div.appendChild(online);

// create the chat iframe and put it in the container div
var iframe = document.createElement('iframe');
iframe.src = url + "chat/" + chat_id;
div.appendChild(iframe);

// write the div to the bottom of the document
document.body.appendChild(div);

var online    = document.getElementById('online');
var live_chat = document.getElementById('live-chat');
online.onclick = function() {
    live_chat.classList.toggle('active');
}
