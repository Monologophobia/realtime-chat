@extends('layouts.app')

@section('content')
<h1>Login</h1>
<form method="POST" action="{{ route('login') }}" class="col s12">
    {{ csrf_field() }}
    <div class="row">
        <div class="input-field col s12">
            <input id="email" type="email" name="email" value="{{ old('email') }}" class="validate" required autofocus />
            <label for="email">Email</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <input id="password" type="password" name="password" class="validate" required />
            <label for="password">Password</label>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <label>Remember Me</label>
            <div class="switch">
                <label>
                No
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} />
                    <span class="lever"></span>
                Yes
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12">
            <p>&nbsp;</p>
            <button class="btn waves-effect waves-dark" type="submit">Login<i class="material-icons right">person</i></button>

            {{--<a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>--}}
        </div>
    </div>
</form>
@endsection
