@extends('layouts.chat')

@section('content')

<div class="container" id="customer">

    @if ($online)
    
    <div id="conversation">
        <p>Send us a message and we'll reply as soon as possible.</p>
        <?php
        if (isset($conversation) && count($conversation) > 0) {
            foreach ($conversation as $message) {
                $class = "right-align";
                if ($message->from['name'] == $chat->name) {
                    $class = "left-align";
                }
                echo '<div class="message card-panel ' . $class . '">' . $message->message . '<br /><small><strong>' . $message->from['name'] . '</strong>, ' . date('H:i', strtotime($message->date)) . '</small></div>';
            }
        }
        ?>
    </div>

    <form id="reply-form">
        {{ csrf_field() }}
        <input type="hidden" name="channel" value="{{ $chat->channel }}" />
        <div class="row">
            <div class="input-field col s12">
            <input type="text" name="message" autofocus required />
        </div>
    </form>

    @else
    <p class="offline">There is no one online</p>

    @endif
</div>
@endsection

@section('scripts')
    @if ($online)
<script>

    var conversation = document.getElementById('conversation');
    var reply_form   = document.getElementById('reply-form')

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('{{ $host->pusher_app_key }}', {
        cluster: 'eu',
        encrypted: true,
        authEndpoint: '/chat/{{ $host->pusher_app_id }}/chat/pusher/auth/customer',
        auth: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }
    });

    var channel = pusher.subscribe('{{ $chat->channel }}');

    // what happens when we send or receive a message
    channel.bind('message', function(data) {
        var from  = "right-align";
        var sound = new Audio("/audio/to.mp3");
        if (data.name == "{{ $chat->name }}") {
            from  = "left-align";
            sound = new Audio("/audio/from.mp3");
        }
        $(conversation).append('<div class="message card-panel ' + from + '">' + data.message + '<br /><small><strong>' + data.name + '</strong>, ' + data.time + '</small></div>');
        $('#customer').animate({ scrollTop: $('#customer').prop("scrollHeight")}, 1000);
        sound.play();
    });

    // what happens when the admin terminates the connection
    channel.bind('disconnect', function(data) {
        closeChannel('{{ $chat->channel }}');
    });
    channel.bind('pusher:member_removed', function(data) {
        closeChannel('{{ $chat->channel }}');
    });

    function closeChannel(channel) {
        pusher.unsubscribe(channel);
        $('#conversation').append('<div class="message card-panel alert">This conversation has ended</div>')
        $('form input').remove();
        $.ajax({
            url: '/chat/{{ $host->pusher_app_id }}/chat/close',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    $('form').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/chat/{{ $host->pusher_app_id }}/chat',
            method: 'POST',
            data: $(this).serializeArray(),
        })
        .done(function(msg) {
            $('form').find('input[name="message"]').val('');
        });
    });
</script>
    @endif
@endsection
