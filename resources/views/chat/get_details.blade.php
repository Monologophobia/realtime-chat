@extends('layouts.chat')

@section('content')

<div class="container" id="customer">

    @if ($online)
    <form id="start-form" method="POST">
        {{ csrf_field() }}
        <input type="email" name="email" />
        <div class="row">
            <div class="input-field col s12">
                <input type="text" name="name" rclass="validate" equired autofocus />
                <label>Your Name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input type="email" name="emailaddress" class="validate" required />
                <label>Your Email</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button type="submit" class="btn">Start Chat</button>
            </div>
        </div>
    <form>

    @else
    <p>There is no one online</p>

    @endif
</div>
@endsection
