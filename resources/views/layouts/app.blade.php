<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Live Chat') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.0/css/materialize.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <nav>
        <div class="container">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo"><img src="/images/logo.svg" alt="Live Chat" /></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    @if (Auth::check())
                    <li class="{{ Route::currentRouteNamed('chats') ? 'active' : '' }}"><a href="{{ route('chats') }}" title="Chats">Chats</a></li>
                    <li class="{{ Route::currentRouteNamed('old-chats') ? 'active' : '' }}"><a href="{{ route('old-chats') }}" title="Old Chats">Old Chats</a></li>
                    <li class="{{ Route::currentRouteNamed('settings') ? 'active' : '' }}"><a href="{{ route('settings') }}" title="Settings">Settings</a></li>
                    <li><a href="{{ route('logout') }}" title="Settings">Logout</a></li>
                    @else
                    <li><a href="{{ route('chats') }}" title="Login">Login</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
        
    <div class="container">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.0/js/materialize.min.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

    @yield('scripts')

</body>
</html>
