@extends('layouts.app')

@section('content')
<h1 class="center-align">Live Chats for {{ $user->name }}</h1>
<div class="divider"></div>

@if (isset($error))
<div class="card-panel red darken-2 white-text">{{ $error }}</div>
@endif

@if (isset($pusher))
<main></main>
@endif

@endsection

@section('scripts')
@if (isset($pusher))
<script>
$(document).ready(function() {

    Notification.requestPermission().then(function(result) {
        console.log(result);
    });

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('{{ $user->pusher_app_key }}', {
        cluster: 'eu',
        encrypted: true,
        auth: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }
    });

    var channels = [];

    // subscribe to presence channel so we know we're online
    channels["{{ $presence_channel }}"] = pusher.subscribe('{{ $presence_channel }}');

    // subscribe to the user pool channel so we can get notifications of new rooms
    channels["{{ $user_pool_channel }}"] = pusher.subscribe('{{ $user_pool_channel }}');
    channels["{{ $user_pool_channel }}"].bind('message', function(data) {
        if (!channels[data.message]) insertChat(pusher, channels, data.message, data)
    });

@foreach ($channels['channels'] as $channel => $conversation)
    @if ($channel != $presence_channel && $channel != $user_pool_channel)

    insertChat(pusher, channels, "{{ $channel }}", <?= json_encode($conversation) ?>);

    @endif
@endforeach

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '.end', function(e) {
        e.preventDefault();
        var parent  = $(this).closest('.chat');
        var channel = $(parent).data('channel');
        $.ajax({
            url: '/channel/close',
            method: "POST",
            data: { 'channel': channel }
        })
        .done(function(msg) {
            removeConversation(pusher, channels, channel);
        });
    });

    $(document).on('submit', '.send', function(e) {
        e.preventDefault();
        var parent  = $(this).closest('.chat');
        var channel = $(parent).data('channel');
        var message = $(parent).find('input[name="message"]').val();
        $.ajax({
            url: '/channel/send',
            method: "POST",
            data: { 'channel': channel, 'message': message }
        })
        .done(function( msg ) {
            $(parent).find('input[name="message"]').val('').focus();
        });
    });

    const original_title = document.title;
    // reset title on refocus
    document.addEventListener("visibilitychange", function() {
        if (document["visibilityState"] == 'visible') {
            document.title = original_title;
        }
    }, false);

});

function insertChat(pusher, channels, channel, conversation) {

    $('main').append(createChatCard(channel, conversation));

    var conversation_content = $('.chat[data-channel="' + channel + '"] .conversation');
    $(conversation_content).animate({ scrollTop: $(conversation_content).prop("scrollHeight")}, 1000);

    channels[channel] = pusher.subscribe(channel);

    // what to do when we receive a message
    channels[channel].bind('message', function(data) {
        var sound = new Audio("/audio/to.mp3");
        if (data.name != "{{ $user->name }}") {
            sound = new Audio("/audio/from.mp3");
            spawnNotification(data.message, 'New Message from ' + data.name);
        }
        $(conversation_content).append(createMessage(data));
        scrollConversation(conversation_content);
        sound.play();
        // if the window is not in focus, change the title text
        if (document["visibilityState"] != 'visible' || document.hidden) {
            document.title = 'UNREAD MESSAGE';
        }
    });

    var close_timer = [];

    // what to do when we receive a customer disconnect
    channels[channel].bind('pusher:member_removed', function(data) {
        $(conversation_content).append('<div class="message card-panel alert"><strong>Customer has left conversation. Closing in 10 seconds unless they reconnect.</strong></div>')
        scrollConversation(conversation_content);
        close_timer[channel] = setTimeout(function() {
            removeConversation(pusher, channels, channel);
        }, 10000);
    });

    // what to do when we receive a customer reconnect
    channels[channel].bind('pusher:member_added', function(data) {
        clearTimeout(close_timer[channel]);
        $(conversation_content).append('<div class="message card-panel info"><strong>Customer has rejoined conversation.</strong></div>')
        scrollConversation(conversation_content);
    });

    // what to do when we receive a disconnect
    channels[channel].bind('disconnect', function(data) {
        removeConversation(pusher, channels, channel);
    });

}

function createChatCard(channel, conversation, name, email) {
    var messages = '';
    var name     = '';
    var email    = '';
    if (conversation.name) name = conversation.name;
    if (conversation.email) email = conversation.email;
    for (var i = 0; i < conversation.length; i++) {
        if (conversation[i]['from']['name'] != "<?= $user->name; ?>") {
            if (!name)  name  = conversation[i]['from']['name'];
            if (!email) email = conversation[i]['from']['email'];
        }
        messages += createMessage(conversation[i]);
    }
    var chat =
    '<div class="card chat" data-channel="' + channel + '">' +
        '<div class="card-title">' + name + ' <small>' + email + '</small></div>' +
        '<div class="card-content">' +
            '<div class="conversation">' +
                messages +
            '</div>' +
        '</div>' +
        '<div class="card-action">' +
            '<form class="send">' +
                '<div class="row">' +
                    '<div class="input-field col s12">' +
                        '<input name="message" type="text" class="validate" required />' +
                        '<label for="message">Reply</label>' +
                    '</div>' +
                '</div>' +
            '</form>' +
            '<a href="#" class="end">End this conversation</a>' +
        '</div>' +
    '</div>';
    return chat;
}

function removeConversation(pusher, channels, channel) {
    pusher.unsubscribe(channel);
    delete channels[channel];
    $('.chat[data-channel="' + channel + '"]').fadeOut(300, function() {
        $(this).remove();
    });
}

function createMessage(conversation) {
    var from = (conversation['name'] ? conversation['name'] : conversation['from']['name']);
    var date;
    if (conversation['time']) {
        date = conversation['time'];
    }
    else {
        date = new Date(conversation['date']);
        date = date.getHours() + ':' + date.getMinutes();
    }
    var css_class = "right-align";
    if (from == "<?= $user->name; ?>") {
        css_class = "left-align";
    }
    return '<div class="message card-panel ' + css_class + '">' + conversation['message'] + '<br /><small><strong>' + from + '</strong>, ' + date + '</small></div>';
}

function scrollConversation(conversation_content) {
    $(conversation_content).animate({ scrollTop: $(conversation_content).prop("scrollHeight")}, 1000);
}

function spawnNotification(body, title) {
    var options = {
        body: body
    }
    var n = new Notification(title, options);
    setTimeout(n.close.bind(n), 10000);
}

</script>
@endif
@endsection