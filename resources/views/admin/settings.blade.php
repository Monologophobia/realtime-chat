@extends('layouts.app')

@section('content')
<h1>Settings</h1>
<form method="POST" action="/settings" class="col s12">
    {{ csrf_field() }}
    <div class="row">
        <div class="input-field col s12">
            <input id="name" name="name" type="text" value="{{ $user->name }}" class="validate" required />
            <label for="name">Display Name</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <input id="pusher_app_key" name="pusher_app_key" type="text" value="{{ $user->pusher_app_key }}" class="validate" required />
            <label for="pusher_app_key">Pusher App Key</label>    
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <input id="pusher_app_secret" name="pusher_app_secret" type="text" value="{{ $user->pusher_app_secret }}" class="validate" required />
            <label>Pusher App Secret</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <input id="pusher_app_id" name="pusher_app_id" type="text" value="{{ $user->pusher_app_id }}" class="validate" required />
            <label>Pusher App ID</label>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <button class="btn waves-effect waves-dark" type="submit">Save<i class="material-icons right">save</i></button>
        <div class="col s12">
    </div>
</form>
@endsection
