@extends('layouts.app')

@section('content')
<h1 class="center-align">Old Chats for {{ $user->name }}</h1>
<p class="center-align">Chat history will be kept for 7 days and then automatically deleted</p>
<div class="divider"></div>

@foreach ($chats as $channel => $thread)
<div class="card chat" data-channel="{{ $channel }}">
    <span class="card-title">{{ $thread[0]->from['name'] }} <small>{{ $thread[0]->from['email'] }}<br />{{ date('Y-m-d', strtotime($thread[0]->date)) }}</small></span>
    <div class="card-content">
        <div class="conversation">
    @foreach ($thread as $message)
        @php
        $from = "left-align";
        if ($user->name == $message->from['name']) {
            $from = "right-align";
        }
        @endphp
            <div class="message card-panel {{ $from }}">{{ $message->message }}<br /><small><strong>{{ $message->from['name'] }}</strong>, {{ date('H:i', strtotime($message->date)) }}</small></div>
    @endforeach
        </div>
    </div>
    <div class="card-action">
        <a href="#" class="delete">Delete this conversation</a>
    </div>
</div>

@endforeach

@endsection

@section('scripts')
<script>
$(document).ready(function() {

    $('.conversation').each(function() {
        $(this).animate({ scrollTop: $(this).prop("scrollHeight")}, 1000);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.delete').click(function(e) {
        e.preventDefault();
        var parent  = $(this).closest('.chat');
        var channel = $(parent).data('channel');
        $.ajax({
            url: '/chats/old',
            method: "POST",
            data: { 'channel': channel }
        })
        .done(function(msg) {
            $(parent).fadeOut(300, function() {
                $(this).remove();
            });
        });
    });

});
</script>
@endsection