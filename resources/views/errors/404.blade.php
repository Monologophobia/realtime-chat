@extends('layouts.app')

@section('content')
<h1>404</h1>
<p>{{ $exception->getMessage() }}</p>
@endsection
