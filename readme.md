## One to One realtime chat system

An experiment to build a realtime chat system using Pusher and Laravel. Allows logged in users to talk to other logged in or not-logged in users. If the person is not logged in, it will ask for a name and email.

Records chats and retrieves them if necessary.

Example use case, live chat on website.
