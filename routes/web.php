<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
// $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
// $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
// $this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/chats', 'Admin@index')->middleware('auth')->name('chats');

Route::get('/chats/old', 'Admin@oldChats')->middleware('auth')->name('old-chats');
Route::post('/chats/old', 'Admin@deleteOldChat')->middleware('auth');

Route::get('/settings', 'Admin@settings')->middleware('auth')->name('settings');
Route::post('/settings', 'Admin@saveSettings')->middleware('auth');

Route::post('/channel/send', 'Admin@sendMessage')->middleware('auth');
Route::post('/channel/close', 'Admin@closeChannel')->middleware('auth');

Route::get('/chat/{app_id}', 'Customers@index');
Route::post('/chat/{app_id}', 'Customers@setDetails');
Route::get('/chat/{app_id}/chat', 'Customers@chat')->name('chat');
Route::post('/chat/{app_id}/chat', 'Customers@sendMessage')->middleware('throttle');
Route::post('/chat/{app_id}/chat/close', 'Customers@closeChat');

Route::post('/pusher/auth', 'Admin@authPresence')->middleware('auth');
Route::post('/chat/{app_id}/chat/pusher/auth/customer', 'Customers@authPresence');