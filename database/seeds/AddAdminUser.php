<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class AddAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'name'              => 'Admin User',
            'email'             => 'phil@monologophobia.com',
            'password'          => bcrypt('supersafepassword'),
            'pusher_app_id'     => 'pusher_app_id',
            'pusher_app_key'    => 'supersafepusherkey',
            'pusher_app_secret' => 'supersafepushersecret',
            'is_admin'          => true
        ]);
    }
}
