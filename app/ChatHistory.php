<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatHistory extends Model {

    protected $table   = 'chat_histories';
    public $timestamps = false;

    protected $casts = [
        'from' => 'array',
    ];

    public static function saveHistory($channel, $message, $user, $host, $date) {

        $from = ['name' => $user->name, 'email' => $user->email];
        if (isset($user->id)) $from['user_id'] = $user->id;

        $chat_history          = new ChatHistory;
        $chat_history->host_id = $host->id;
        $chat_history->channel = $channel;
        $chat_history->from    = $from;
        $chat_history->message = $message;
        $chat_history->date    = $date;
        $chat_history->save();

    }

}
