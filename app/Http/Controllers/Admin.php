<?php namespace App\Http\Controllers;

use Auth;
use Pusher;
use App\ChatHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Chat;

class Admin extends Controller {

    protected $user;
    protected $pusher;
    protected $chat;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->chat = new Chat($this->user);
            return $next($request);
        });
    }

    public function index() {

        try {
            $data = [
                'user'     => $this->user,
                'pusher'   => $this->chat->getPusher(),
                'channels' => $this->chat->getChannels(),
                'presence_channel'  => $this->chat->presence_channel,
                'user_pool_channel' => $this->chat->user_pool_channel
            ];
        }
        catch (\Exception $e) {
            $data = [
                'user'  => $this->user,
                'error' => $e->getMessage()
            ];
        }

        return view('admin/chats')->with($data);

    }

    public function settings() {
        return view('admin/settings')->with('user', $this->user);
    }

    /**
     * Save pusher details
     * @return Redirect
     */
    public function saveSettings(Request $request) {
        $user = Auth::user();
        $user->name              = $request->input('name');
        $user->pusher_app_key    = $request->input('pusher_app_key');
        $user->pusher_app_secret = $request->input('pusher_app_secret');
        $user->pusher_app_id     = $request->input('pusher_app_id');
        $user->save();
        return redirect()->route('chats')->with('message', 'Settings Saved');
    }

    public function authPresence(Request $request) {
        if ($this->user) {
            echo $this->chat->authPresence($request->input('channel_name'), $request->input('socket_id'), $this->user->id, ['name' => $this->user->name]);
        }
        else {
            abort(403);
        }
    }

    public function sendMessage(Request $request) {
        try {
            $channel = $request->input('channel');
            $message = $request->input('message');
            $send    = $this->chat->sendMessage($channel, $message, $this->user);
            return json_encode($send);
        }
        catch (\Exception $e) {
            return json_encode($e);
        }
    }

    public function closeChannel(Request $request) {
        try {
            $channel = $request->input('channel');
            $close   = $this->chat->closeChannel($channel);
            return json_encode($close);
        }
        catch (\Exception $e) {
            return json_encode($e);
        }
    }

    public function oldChats() {
        $chats = ChatHistory::where('host_id', $this->user->id)->orderBy('date', 'asc')->get();
        $array = [];
        foreach ($chats as $chat) {
            if (!isset($array[$chat->channel])) $array[$chat->channel] = [];
            $array[$chat->channel][] = $chat;
        }
        return view('admin/old_chats')->with(['chats' => $array, 'user' => $this->user]);
    }

    public function deleteOldChat(Request $request) {
        $channel = filter_var($request->input('channel'), FILTER_SANITIZE_STRING);
        return ChatHistory::where('host_id', $this->user->id)->where('channel', $channel)->delete();
    }

}
