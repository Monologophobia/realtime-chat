<?php namespace App\Http\Controllers;

use Pusher;
use App\ChatHistory;

class Chat {

    protected $host;
    protected $pusher;
    public $presence_channel  = 'presence-admin';
    public $user_pool_channel = 'user-pool';

    public function __construct($user) {
        $this->host   = $user;
        $this->pusher = $this->instantiatePusher($user);
    }

    /**
     * Instantiates Pusher with the user supplied ids and keys
     * @param User Model
     * @return Pusher Object
     */
    private function instantiatePusher($user) {
        if ($user->pusher_app_id && $user->pusher_app_key && $user->pusher_app_secret) {
            return new Pusher\Pusher(
                $user->pusher_app_key,
                $user->pusher_app_secret,
                $user->pusher_app_id,
                ['cluster' => 'eu', 'encrypted' => true]
            );
        }
    }

    public function getPusher() {
        return $this->pusher;
    }

    /**
     * Authorise a person on a presence channel
     * @param String channel
     * @param String socket
     * @param String User ID
     * @param Array optional data
     */
    public function authPresence($channel, $socket, $id, $data) {
        return $this->pusher->presence_auth($channel, $socket, $id, $data);
    }

    /**
     * Checks whether an admin is on the presence channel
     * @return boolean
     */
    public function isAdminPresent() {
        $response = $this->pusher->get("/channels/" . $this->presence_channel . "/users");
        $users = $this->decodeResponse($response);
        if (count($users['users']) > 0) return true;
        return false;
    }

    public function getChannels() {
        $response = $this->pusher->get('/channels');
        $response = $this->decodeResponse($response);
        foreach ($response as $channels) {
            foreach ($channels as $channel => $value) {
                $response['channels'][$channel] = ChatHistory::where('host_id', $this->host->id)->where('channel', $channel)->get();
            }
        }
        return $response;
    }

    public function sendMessage($channel, $message, $user) {

        $channel = filter_var($channel, FILTER_SANITIZE_STRING);
        $message = filter_var(htmlentities($message), FILTER_SANITIZE_STRING);
        $date    = date('Y-m-d H:i:s');

        ChatHistory::saveHistory($channel, $message, $user, $this->host, $date);

        return $this->pusher->trigger($channel, 'message', ['message' => $message, 'name' => $user->name, 'email' => $user->email, 'time' => date('H:i', strtotime($date))]);

    }

    public function closeChannel($channel) {
        $channel = filter_var($channel, FILTER_SANITIZE_STRING);
        return $this->pusher->trigger($channel, 'disconnect', []);
    }

    /**
     * Decodes the pusher response and returns an object
     * @param Pusher Response
     * @return Object
     * @throws Exception
     */
    private function decodeResponse($response) {
        if ($response['status'] == 200) {
            return json_decode($response['body'], true);
        }
        throw new \Exception ("Pusher API is unavailable");
    }

}
