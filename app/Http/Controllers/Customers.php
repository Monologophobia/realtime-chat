<?php namespace App\Http\Controllers;

use Auth;
use Crypt;
use Route;
use Pusher;
use App\User;
use App\ChatHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Chat;

class Customers extends Controller {

    protected $host;
    protected $chat;

    public function __construct() {

        $pusher_app_id = Route::current()->parameters();
        $pusher_app_id = filter_var($pusher_app_id['app_id'], FILTER_SANITIZE_STRING);

        $this->host    = User::where('pusher_app_id', $pusher_app_id)->first();
        if (!$this->host) abort(404);

        $this->chat = new Chat($this->host);

    }

    public function index() {
        $user = Auth::getUser();
        if ($user) return redirect()->route('chat');
        $details = $this->getDetails();
        if ($details->name) return redirect()->to('/chat/' . $this->host->pusher_app_id . '/chat');
        return view('chat/get_details', ['online' => $this->chat->isAdminPresent()]);
    }

    public function setDetails(Request $request) {
        // if the honeypot's filled, 404
        $email = filter_var($request->input('email'), FILTER_SANITIZE_EMAIL);
        if ($email) abort(404);
        $email = filter_var($request->input('emailaddress'), FILTER_SANITIZE_EMAIL);
        $name  = filter_var($request->input('name'), FILTER_SANITIZE_STRING);
        session(['email' => $email, 'name' => $name]);
        return redirect()->to('/chat/' . $this->host->pusher_app_id . '/chat');
    }

    private function getDetails() {
        $user = Auth::user();
        if ($user) {
            return (object) ['name' => $user->name, 'email' => $user->email, 'channel' => 'presence-' . $user->id];
        }
        return (object) ['name' => session('name'), 'email' => session('email'), 'channel' => 'presence-' . md5((session('name') . session('email')))];
    }

    public function chat() {

        $details = $this->getDetails();
        if (!$details->name) return redirect()->to('/chat/' . $this->host->pusher_app_id);

        $data = [
            'host'   => $this->host,
            'chat'   => $this->getDetails(),
            'pusher' => $this->chat->getPusher(),
            'online' => $this->chat->isAdminPresent()
        ];
        $data['conversation'] = ChatHistory::where('channel', $data['chat']->channel)->where('host_id', $this->host->id)->get();

        return view('chat/chat')->with($data);

    }

    public function authPresence(Request $request) {
        try {
            $details      = $this->getDetails();
            // send a notification of the new channel to the user pool so admins can subscribe
            $this->chat->sendMessage($this->chat->user_pool_channel, $details->channel, $details);
            echo $this->chat->authPresence($details->channel, $request->input('socket_id'), uniqid(), ['name' => $details->name]);
        }
        catch (\Exception $e) {
            abort(403);
        }
    }

    public function sendMessage(Request $request) {

        $channel = $request->input('channel');
        $message = $request->input('message');
        if (!$message || !$channel) return;
        $send    = $this->chat->sendMessage($channel, $message, $this->getDetails());
        return json_encode($send);

    }

    public function closeChat(Request $request) {
        $user = Auth::user();
        if ($user) Auth::logout();
        $request->session()->flush();
    }

}
